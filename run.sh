#GHC=$HOME/ghc-8.10.1/bin/ghc.exe
GHC=$HOME/ghc/_build/stage1/bin/ghc.exe

run() {
    $GHC -fforce-recomp -rtsopts -g3 \
        -ddump-to-file -ddump-simpl -ddump-stg -ddump-cmm -ddump-ds -dsuppress-coercions -ddump-cmm-verbose -ddump-cmm-from-stg -dsuppress-idinfo  -ddump-asm-regalloc -ddump-asm \
	-ddump-cmm-verbose-by-proc \
        -dcore-lint -dstg-lint -dcmm-lint \
        Main.hs  ffi.c \
        $@
}

cat >gdb-test <<EOF
break func
commands
info reg
bt
x/16a \$rsp
cont
end

break exit
commands
quit
y
end

run
EOF

try() {
    name=$1
    shift
    args=$@
    run -dumpdir $name -o $name.exe $args
    ./$name.exe > $name/log
    grep -s "corrupted" $name/log && echo "$name bad"

    #cat gdb-test | gdb --args ./$name.exe +RTS -I0 >$name/gdb.log
}

try o0 -O0
try o1 -O1
try o1-nosink -O1 -fno-cmm-sink

